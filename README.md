# Go - TypeScript Type Synchronization Example

## Steps

1. download [tygo](https://github.com/gzuidhof/tygo) - go install github.com/gzuidhof/tygo@latest
2. make sure go/bin is in your path, so that you can run tygo from the terminal
3. cd into backend
4. run `tygo generate`

This will read the exported types from the `models` package in the backend go project,
generate the TypeScript types and insert them into the frontend project

In this example:
1. TS interfaces are generated
2. fields are renamed from PascalCase to camelCase
3. go-specific types are mapped to TS types (time.Time -> string)
4. doc-comments are automatically transferred to TS
