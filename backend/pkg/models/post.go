package models

// Post created by a user
type Post struct {
	ID      uint32 `json:"id"`
	Title   string `json:"title"`
	Content string `json:"content"`
	// Links to models.User.ID
	AuthorID uint32 `json:"authorId"`
}
